require("seneca")()
    .use("seneca-amqp-transport")
    .use("user")
    .use("basic")
    .use("entity")
    .use("mongo-store", {
        uri: process.env.USER_STORE_URI
        //uri: "mongodb://heroku_3sgpxjcq:qchhtujcr0uj8cfgtlvh71sr7k@ds139954.mlab.com:39954/heroku_3sgpxjcq"
    })
    .listen({
        type: "amqp",
        pin: "role:user,cmd:*",
        url: process.env.BROKER_URL
        //url: "amqp://itKFic0X:GpgyM6s7yizu8bn77ZZFJabp0Mn0xTET@brown-kingcup-357.bigwig.lshift.net:10225/_8M1QgJ3n0Ju"
    });