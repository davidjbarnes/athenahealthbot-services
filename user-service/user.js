module.exports = function () {
    this.add("role:user,cmd:get", get);
    this.add("role:user,cmd:create", create);

    function get(req, done) {
        var user = this.make$("User");
        user.load$({id: req.id}, function (err, entity) {
            return done(err, { result: entity, when: Date.now() });
        });
    };

    function create(req, done) {
        var user = this.make$("User");
        user.name = req.name;
        user.save$((err, savedUser) => {
          done(null, { result: savedUser, when: Date.now() });
        });
    }
};