Description:
Bringing together the Athena Health API with the Microsoft Bot Framework using a Microservices architecture.

Run the Project:
$ heroku local;
$ heroku ps:scale user-service=1, conversation-service=1;