require("seneca")()
.use("seneca-amqp-transport")
.use("conversation")
.use("basic")
.use("entity")
.use("mongo-store", {
    uri: process.env.USER_STORE_URI
})
.listen({
    type: "amqp",
    pin: "role:conversation,cmd:*",
    url: process.env.BROKER_URL
});