module.exports = function () {
    this.add("role:conversation,cmd:log", log);

    function log(req, done) {
        var conversation = this.make$("Conversation");
        conversation.text = req.text;
        conversation.save$((err, savedConversation) => {
          done(err, { result: savedConversation, when: Date.now() });
        });
    }
};