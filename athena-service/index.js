require("seneca")()
.use("seneca-amqp-transport")
.use("auth")
.use("patient")
.use("basic")
.use("entity")
.use("mongo-store", {
    uri: process.env.USER_STORE_URI
})
.listen({
    type: "amqp",
    pin: "role:athena,cmd:*",
    url: process.env.BROKER_URL
});