var request = require("request");

module.exports = function() {
    this.add("role:athena,cmd:auth", auth);

    function auth(req, done) {
        var options = {
            method: "POST",
            url: "https://api.athenahealth.com/oauthpreview/token",
            headers: {
                "authorization": "Basic " + new Buffer(req.key + ":" + req.secret).toString("base64"),
                "content-type": "application/x-www-form-urlencoded"
            },
            auth: {
                "user": req.key,
                "pass": req.secret,
                "sendImmediately": true
            },
            form: {
                "grant_type": "client_credentials"
            }
        };
        
        request(options, (err, response, body) => {
            done(err, { result:body, when:Date.now() });
        });
    }
};