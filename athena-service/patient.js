var request = require("request");

module.exports = function() {
    this.add("role:athena,cmd:createPatient", create);
    this.add("role:athena,cmd:getPatient", get);

    function create(req, done) {
        var options = {
            method: "POST",
            url: "https://api.athenahealth.com/preview1/195900/patients",
            headers: {
                "authorization": "Bearer ".concat(req.access_token),
                "content-type": "application/x-www-form-urlencoded"
            },
            form: {
                "firstname": req.patient.firstname,
                "lastname": req.patient.lastname,
                "departmentid": req.patient.departmentid,
                "dob": req.patient.dob,
                "zip": req.patient.zip,
            }
        };
        
        request(options, (err, response, body) => {
            done(err, { result:JSON.parse(body)[0].patientid, when:Date.now() });
        });
    }

    function get(req, done) {
        var options = {
            method: "GET",
            url: "https://api.athenahealth.com/preview1/195900/patients/".concat(req.patientId),
            headers: {
                "authorization": "Bearer ".concat(req.access_token),
            }
        };
        
        request(options, (err, response, body) => {
            done(err, { result:JSON.parse(body), when:Date.now() });
        });
    }
};